package formulaireMVC;

import static org.junit.Assert.assertEquals;

public class Test {

    @Test
    public void test_Verification_Client1_Prix_Produit1() {
        assertEquals(24, produit1.prix);
    }

    @Test
    public void test_Verification_Client1_Prix_Produit2() {
        assertEquals(37, produit2.prix);
    }

    @Test
    public void test_Verification_Client1_Prix_Produit3() {
        assertEquals(56, produit3.prix);
    }

    @Test
    public void test_Verification_Client2_Prix_Produit1() {
        assertEquals(10, produit1.prix);
    }

    @Test
    public void test_Verification_Client2_Prix_Produit2() {
        assertEquals(20, produit2.prix);
    }

    @Test
    public void test_Verification_Client2_Prix_Produit3() {
        assertEquals(30, produit3.prix);
    }

    @Test
    public void test_Verification_Client3_Prix_Produit1() {
        assertEquals(50, produit1.prix);
    }

    @Test
    public void test_Verification_Client3_Prix_Produit2() {
        assertEquals(100, produit2.prix);
    }

    @Test
    public void test_Verification_Client3_Prix_Produit3() {
        assertEquals(150, produit3.prix);
    }

}
