<%@page pageEncoding="UTF-8" isErrorPage="true" contentType="text/html" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
  <head>
  	<meta charset="UTF-8">
    <title>Java EE</title>
    <style type="text/css">
    	form > div {
    		padding: .5em;
    	}
    	
    	div > label:first-child {
    		display: inline-block;
			min-width: 18em;
		}
		
		.error {
			color: red;
		}
    </style>
  </head>
  <body>

	<form method="post" accept-charset="utf-8">
		<div>
			<label for="produit1">Produit 1</label>
			<label for="prix1">Prix 1 : 24</label>
		</div>
		<div>
			<label for="produit2">Produit 2</label>
			<label for="prix2">Prix 2 : 37</label>
		</div>
		<div>
			<label for="produit3">Produit 3</label>
			<label for="prix3">Prix 3 : 56</label>
		</div>
	</form>

	<div>
	  	<a href="<c:url value="/"/>">Retour à l'accueil</a>  
	</div>
  
  </body>
</html>